import { Injectable } from '@angular/core';
import { residusABI } from './residusABI';
import web3 from 'web3';

const contractAddress = '0x40061440badf7db16e0b9fd66e93b16ab17cd498';

@Injectable({
  providedIn: 'root'
})
export class EthresidusService {
  web3js: web3;
  residusContract;
  ecos: number;

  constructor() { }

  updateEcoLevel() {
    this.web3js = new web3(new web3.providers.HttpProvider('https://rinkeby.infura.io/v3/718c05adc4d44d4e8ba562f161bb6a69'));
    this.residusContract = new this.web3js.eth.Contract(residusABI, contractAddress);
    this.residusContract.methods.getUserEcos(1).call().then((result) => {
      this.ecos = result;
    });
  }
}
