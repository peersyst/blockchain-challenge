import { Component } from '@angular/core';
import {AlertController} from '@ionic/angular';
import {EthresidusService} from '../ethresidus.service';

@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss']
})
export class Tab4Page {
  alertController: AlertController;

  constructor(alertController: AlertController, public es: EthresidusService) {
    this.alertController = alertController;
  }


  showAlert(message) {
    this.alertController.create({
      header: 'Aconseguit!',
      message: message,
      buttons: ['D\'acord']
    }).then(alert => { alert.present(); });
  }

}
