import { Component } from '@angular/core';
import { residusABI } from '../residusABI';
import web3 from 'web3';
import {EthresidusService} from '../ethresidus.service';

const contractAddress = '0x40061440badf7db16e0b9fd66e93b16ab17cd498';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page {
  web3js: web3;
  residusContract;
  deposits: any[] = [];
  es: EthresidusService;


  constructor(es: EthresidusService) {
      this.es = es;
    this.web3js = new web3(new web3.providers.HttpProvider('https://rinkeby.infura.io/v3/718c05adc4d44d4e8ba562f161bb6a69'));
    this.residusContract = new this.web3js.eth.Contract(residusABI, contractAddress);
    this.residusContract.methods.getUserDeposits(1).call().then((result) => {
      this.deposits = result;
      this.deposits.reverse();
    });
  }


  public doRefresh(refresher) {
      this.es.updateEcoLevel();
      this.web3js = new web3(new web3.providers.HttpProvider('https://rinkeby.infura.io/v3/718c05adc4d44d4e8ba562f161bb6a69'));
      this.residusContract = new this.web3js.eth.Contract(residusABI, contractAddress);
      this.residusContract.methods.getUserDeposits(1).call().then((result) => {
          this.deposits = result;
          this.deposits.reverse();
          if (refresher) refresher.target.complete();
      });
  }

}
