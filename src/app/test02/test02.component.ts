import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test02',
  templateUrl: './test02.component.html',
  styleUrls: ['./test02.component.scss'],
})
export class Test02Component implements OnInit {
    private change;
    constructor() {
        this.ngOnInit();
    }

    ngOnInit() {
        this.change = true;
    }

    changeColor2(x) {
        if (this.change) {
            if (x !== 4) {
                const element = 'incorrecte02' + x;
                document.getElementById(element).style.background = '#DFA43E';
                document.getElementById(element).style.color = 'white';
                document.getElementById('seguent_incorrecte2').style.display = 'block';
            } else {
                document.getElementById('seguent_correcte2').style.display = 'block';
            }
            document.getElementById('correcte2').style.background = '#376f6f';
            document.getElementById('correcte2').style.color = 'white';
        }

        this.change = false;

    }

}