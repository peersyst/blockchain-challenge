import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.scss'],
})
export class TestsComponent implements OnInit {
  private change;
  constructor() {
    this.ngOnInit();
  }

  ngOnInit() {
    this.change = true;
  }

  changeColor(x) {
    if (this.change) {
        if (x !== 4) {
            const element = 'incorrecte0' + x;
            document.getElementById(element).style.background = '#DFA43E';
            document.getElementById(element).style.color = 'white';
            document.getElementById('seguent_incorrecte').style.display = 'block';
        }
        else {
            document.getElementById('seguent_correcte').style.display = 'block';
        }
        document.getElementById('correcte').style.background = '#376f6f';
        document.getElementById('correcte').style.color = 'white';
    }

    this.change = false;

  }

}
