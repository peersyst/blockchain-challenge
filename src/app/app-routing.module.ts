import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {TestsComponent} from './tests/tests.component';
import {InfoQuestionsComponent} from './info-questions/info-questions.component';
import {ChatComponent} from './chat/chat.component';
import {Test02Component} from './test02/test02.component';
import {Test03Component} from './test03/test03.component';


const routes: Routes = [
    { path: '', redirectTo: 'tabs/tab3', pathMatch: 'full' },
    {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'tests', component: TestsComponent },
  { path: 'info-questions', component: InfoQuestionsComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'test02', component: Test02Component },
  { path: 'test03', component: Test03Component }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
