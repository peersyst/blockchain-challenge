import { Component } from '@angular/core';
import {AlertController, LoadingController} from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import {EthresidusService} from '../ethresidus.service';
import {Tab4Page} from '../tab4/tab4.page';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  alertController: AlertController;
  barcodeScanner: BarcodeScanner;
  loadingController: LoadingController;

  constructor(alertController: AlertController, barcodeScanner: BarcodeScanner, loadingController: LoadingController, private es: EthresidusService) {
    this.alertController = alertController;
    this.barcodeScanner = barcodeScanner;
    this.loadingController = loadingController;
  }

  showBarcode() {
    this.alertController.create({
      header: 'Instruccions',
      message: "Per tirar les escombreries cal que t'apropis a un contenidor de la ciutat i escanegis el QR que trobaras al seu davant",
      buttons: [
        {
          text: 'Cancel·lar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Escanejar',
          handler: () => {
            this.barcodeScanner.scan().then(barcodeData => {
              const id = parseInt(barcodeData.text, 0);
              if (!isNaN(id)) {
              this.sendOpenContainer(id);
              } else {
                this.alertController.create({
                  header: 'Error',
                  message: 'El codi escanejat no és correcte',
                  buttons: ['D\'acord']
                }).then(alert => { alert.present(); });
              }
            }).catch(err => {
              console.log('Error', err);
            });
          }
        }
      ]
    }).then(alert => {
      alert.present();
    });
  }

  sendOpenContainer(id) {
    this.loadingController.create().then(loading => {
      loading.present();
      fetch('http://172.86.75.19/deposit?userId=1&containerId=' + id, { method: 'GET' }).then(res => {
        loading.dismiss();
        this.alertController.create({
          header: 'Contenedor desbloquejat',
          message: 'Ja pots dipositar les escombraries',
          buttons: ['D\'acord']
        }).then(alert => { alert.present(); this.es.updateEcoLevel(); });
      });
    });
  }

}
