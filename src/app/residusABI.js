const residusABI = [
    {
        "constant": false,
        "inputs": [
            {
                "name": "_containerId",
                "type": "uint256"
            },
            {
                "name": "_containerType",
                "type": "string"
            }
        ],
        "name": "addContainer",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_userId",
                "type": "uint256"
            },
            {
                "name": "_address",
                "type": "address"
            }
        ],
        "name": "assignAddressToUser",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_userId",
                "type": "uint256"
            },
            {
                "name": "_containerId",
                "type": "uint256"
            },
            {
                "name": "_operationEcos",
                "type": "int256"
            }
        ],
        "name": "deposit",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "newOwner",
                "type": "address"
            }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "name": "containerId",
                "type": "uint256"
            }
        ],
        "name": "OpenContainer",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "name": "previousOwner",
                "type": "address"
            },
            {
                "indexed": true,
                "name": "newOwner",
                "type": "address"
            }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "_userId",
                "type": "uint256"
            }
        ],
        "name": "getUserDeposits",
        "outputs": [
            {
                "components": [
                    {
                        "name": "containerId",
                        "type": "uint256"
                    },
                    {
                        "name": "containerType",
                        "type": "string"
                    },
                    {
                        "name": "operationEcos",
                        "type": "int256"
                    },
                    {
                        "name": "timestamp",
                        "type": "uint256"
                    }
                ],
                "name": "",
                "type": "tuple[]"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "_userId",
                "type": "uint256"
            }
        ],
        "name": "getUserEcos",
        "outputs": [
            {
                "name": "",
                "type": "int256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "owner",
        "outputs": [
            {
                "name": "",
                "type": "address"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    }
];

export { residusABI };
