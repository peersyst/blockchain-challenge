import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test03',
  templateUrl: './test03.component.html',
  styleUrls: ['./test03.component.scss'],
})
export class Test03Component implements OnInit {

    private change;
    constructor() {
        this.ngOnInit();
    }

    ngOnInit() {
        this.change = true;
    }

    changeColor3(x) {
        if (this.change) {
            if (x !== 4) {
                const element = 'incorrecte03' + x;
                document.getElementById(element).style.background = '#DFA43E';
                document.getElementById(element).style.color = 'white';
                document.getElementById('seguent_incorrecte3').style.display = 'block';
            } else {
                document.getElementById('seguent_correcte3').style.display = 'block';
            }
            document.getElementById('correcte3').style.background = '#376f6f';
            document.getElementById('correcte3').style.color = 'white';
        }

        this.change = false;

    }

}