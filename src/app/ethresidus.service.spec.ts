import { TestBed } from '@angular/core/testing';

import { EthresidusService } from './ethresidus.service';

describe('EthresidusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EthresidusService = TestBed.get(EthresidusService);
    expect(service).toBeTruthy();
  });
});
