import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TestsComponent} from './tests/tests.component';
import {InfoQuestionsComponent} from './info-questions/info-questions.component';
import {ChatComponent} from './chat/chat.component';
import {Test02Component} from './test02/test02.component';
import {Test03Component} from './test03/test03.component';


import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import {EthresidusService} from './ethresidus.service';


@NgModule({
  declarations: [
      AppComponent,
      TestsComponent,
      InfoQuestionsComponent,
      ChatComponent,
      Test02Component,
      Test03Component,

  ],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
      EthresidusService,
      BarcodeScanner,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
