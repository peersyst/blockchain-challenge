import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {EthresidusService} from '../ethresidus.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private router: Router, private es: EthresidusService) {
  }

  navigate() {
      this.router.navigate(['(questionaries']);
  }

}
