(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab2-tab2-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tab2/tab2.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tab2/tab2.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Historial\n    </ion-title>\n    <ion-text slot=\"end\">Nivell 99</ion-text>\n    <ion-icon style=\"margin-left: 10px;\" slot=\"end\" src=\"assets/recycle-solid.svg\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n      <ion-card>\n        <ion-list *ngFor=\"let deposit of deposits\">\n          <ion-list-header>{{ deposit.containerType }}</ion-list-header>\n          <ion-item>\n            <ion-icon *ngIf=\"deposit.containerType == 'organic'\" src=\"assets/apple-beaten.svg\"></ion-icon>\n            <ion-icon *ngIf=\"deposit.containerType == 'plastic'\" src=\"assets/plastic-bottle.svg\"></ion-icon>\n            <ion-icon *ngIf=\"deposit.containerType == 'vidre'\" src=\"assets/wine-bottle-solid.svg\"></ion-icon>\n            <ion-icon *ngIf=\"deposit.containerType == 'paper'\" name=\"copy\"></ion-icon>\n            <ion-icon *ngIf=\"deposit.containerType == 'rebuig'\" src=\"assets/dumpster-solid.svg\"></ion-icon>\n            <ion-label>{{ deposit.timestamp * 1000 | date: 'yyyy-MM-dd HH:mm' }}</ion-label>\n            <ion-text *ngIf=\"deposit.operationEcos > 0\">{{ '+' + deposit.operationEcos }}</ion-text>\n            <ion-text *ngIf=\"deposit.operationEcos <= 0\" style=\"color: red !important;\">{{ deposit.operationEcos }}</ion-text>\n            <ion-icon style=\"color:#DFA43E; margin-left: 5px; font-size:16px;\" slot=\"end\" src=\"assets/recycle-solid.svg\"></ion-icon>\n          </ion-item>\n        </ion-list>\n      </ion-card>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/tab2/tab2.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab2/tab2.module.ts ***!
  \*************************************/
/*! exports provided: Tab2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function() { return Tab2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab2.page */ "./src/app/tab2/tab2.page.ts");







var Tab2PageModule = /** @class */ (function () {
    function Tab2PageModule() {
    }
    Tab2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"] }])
            ],
            declarations: [_tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"]]
        })
    ], Tab2PageModule);
    return Tab2PageModule;
}());



/***/ }),

/***/ "./src/app/tab2/tab2.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab2/tab2.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-card {\n  margin-left: 5%;\n  width: 90%;\n  display: block;\n}\n\nion-icon {\n  color: #DFA43E;\n  margin-right: 15px;\n}\n\nion-text {\n  color: #DFA43E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZHJpYWNhcnJlcmEvR0lUL2Jsb2NrY2hhaW4tY2hhbGxlbmdlL3NyYy9hcHAvdGFiMi90YWIyLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdGFiMi90YWIyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtBQ0NGOztBREVBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FERUE7RUFDRSxjQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC90YWIyL3RhYjIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNhcmR7XG4gIG1hcmdpbi1sZWZ0OiA1JTtcbiAgd2lkdGg6IDkwJTtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbmlvbi1pY29ue1xuICBjb2xvcjogI0RGQTQzRTtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuXG5pb24tdGV4dHtcbiAgY29sb3I6ICNERkE0M0U7XG59XG4iLCJpb24tY2FyZCB7XG4gIG1hcmdpbi1sZWZ0OiA1JTtcbiAgd2lkdGg6IDkwJTtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbmlvbi1pY29uIHtcbiAgY29sb3I6ICNERkE0M0U7XG4gIG1hcmdpbi1yaWdodDogMTVweDtcbn1cblxuaW9uLXRleHQge1xuICBjb2xvcjogI0RGQTQzRTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/tab2/tab2.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab2/tab2.page.ts ***!
  \***********************************/
/*! exports provided: Tab2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2Page", function() { return Tab2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _residusABI__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../residusABI */ "./src/app/residusABI.js");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! web3 */ "./node_modules/web3/src/index.js");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(web3__WEBPACK_IMPORTED_MODULE_3__);




var contractAddress = '0x40061440badf7db16e0b9fd66e93b16ab17cd498';
var Tab2Page = /** @class */ (function () {
    function Tab2Page() {
        var _this = this;
        this.deposits = [];
        this.web3js = new web3__WEBPACK_IMPORTED_MODULE_3___default.a(new web3__WEBPACK_IMPORTED_MODULE_3___default.a.providers.HttpProvider('https://rinkeby.infura.io/v3/718c05adc4d44d4e8ba562f161bb6a69'));
        this.residusContract = new this.web3js.eth.Contract(_residusABI__WEBPACK_IMPORTED_MODULE_2__["residusABI"], contractAddress);
        this.residusContract.methods.getUserDeposits(1).call().then(function (result) {
            _this.deposits = result;
            _this.deposits.reverse();
        });
    }
    Tab2Page.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.web3js = new web3__WEBPACK_IMPORTED_MODULE_3___default.a(new web3__WEBPACK_IMPORTED_MODULE_3___default.a.providers.HttpProvider('https://rinkeby.infura.io/v3/718c05adc4d44d4e8ba562f161bb6a69'));
        this.residusContract = new this.web3js.eth.Contract(_residusABI__WEBPACK_IMPORTED_MODULE_2__["residusABI"], contractAddress);
        this.residusContract.methods.getUserDeposits(1).call().then(function (result) {
            _this.deposits = result;
            _this.deposits.reverse();
            refresher.target.complete();
        });
    };
    Tab2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab2',
            template: __webpack_require__(/*! raw-loader!./tab2.page.html */ "./node_modules/raw-loader/index.js!./src/app/tab2/tab2.page.html"),
            styles: [__webpack_require__(/*! ./tab2.page.scss */ "./src/app/tab2/tab2.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], Tab2Page);
    return Tab2Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab2-tab2-module-es5.js.map