(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tab1/tab1.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tab1/tab1.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Aprèn\n    </ion-title>\n      <ion-text slot=\"end\">Nivell 99</ion-text>\n      <ion-icon style=\"margin-left: 10px;\" slot=\"end\" src=\"assets/recycle-solid.svg\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <img src=\"../assets/imgs/check.png\" style=\"float: left; margin: 15px; max-width: 18%;\">\n  <h4 align=\"left\" style=\"color: #454e55; font-weight: bold;\">Informa't i respon els qüestionaris per pujar de nivell ECO</h4>\n\n<ion-card class=\"test01\" [routerLink]=\"['/info-questions']\">\n    <ion-card-header style=\"background-color: #376f6f;\">\n      <ion-card-subtitle style=\"color:white;\">Recollida selectiva</ion-card-subtitle>\n      <ion-card-title style=\"color:white;\">Classificació de residus</ion-card-title>\n    </ion-card-header>\n    \n    <ion-card-content style=\"background-color: #376f6f;\">\n      <ion-label> Preguntes encertades: 10 / 10 </ion-label>\n      <ion-button [routerLink]=\"['/info-questions']\" style=\"float: right; margin: -10px; background-color: #376f6f;\">\n      <ion-icon name=\"arrow-dropright\"></ion-icon> </ion-button>\n    </ion-card-content>\n</ion-card>\n\n<ion-card class=\"test02\" [routerLink]=\"['/info-questions']\">\n    <ion-card-header style=\"background-color: #376f6f;\">\n      <ion-card-subtitle style=\"color:white;\"> Aprèn a reduïr la quantitat de residus que generes </ion-card-subtitle>\n      <ion-card-title style=\"color:white;\"> Reduïr residus </ion-card-title>\n    </ion-card-header>\n    \n    <ion-card-content style=\"background-color: #376f6f;\">\n      <ion-label>  Preguntes encertades: 6 / 10 </ion-label>\n      <ion-button [routerLink]=\"['/info-questinos']\" style=\"float: right; margin: -10px; background-color: #376f6f;\">\n      <ion-icon name=\"arrow-dropright\"></ion-icon> </ion-button>\n    </ion-card-content>\n\n</ion-card>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/tab1/tab1.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1.page */ "./src/app/tab1/tab1.page.ts");







var Tab1PageModule = /** @class */ (function () {
    function Tab1PageModule() {
    }
    Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"] }])
            ],
            declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
        })
    ], Tab1PageModule);
    return Tab1PageModule;
}());



/***/ }),

/***/ "./src/app/tab1/tab1.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n\n.test01 {\n  background-color: beige;\n}\n\n.test02 {\n  background-color: beige;\n}\n\nh5 {\n  width: 90%;\n  margin-left: 10%;\n}\n\nion-card {\n  color: white;\n  background-color: #376f6f;\n}\n\nion-button {\n  --background: transparent;\n}\n\nion-icon {\n  color: #DFA43E;\n  margin-right: 15px;\n}\n\nion-text {\n  color: #DFA43E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZHJpYWNhcnJlcmEvR0lUL2Jsb2NrY2hhaW4tY2hhbGxlbmdlL3NyYy9hcHAvdGFiMS90YWIxLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdGFiMS90YWIxLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLHVCQUFBO0FDQ0Y7O0FERUE7RUFDRSx1QkFBQTtBQ0NGOztBREVBO0VBQ0UsVUFBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FERUE7RUFDQyxZQUFBO0VBQ0EseUJBQUE7QUNDRDs7QURFQTtFQUNDLHlCQUFBO0FDQ0Q7O0FERUE7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7QUNDRjs7QURFQTtFQUNFLGNBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2VsY29tZS1jYXJkIGltZyB7XG4gIG1heC1oZWlnaHQ6IDM1dmg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi50ZXN0MDF7XG4gIGJhY2tncm91bmQtY29sb3I6IGJlaWdlO1xufVxuXG4udGVzdDAye1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBiZWlnZTtcbn1cblxuaDUge1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW4tbGVmdDogMTAlO1xufVxuXG5pb24tY2FyZHtcblx0Y29sb3I6IHdoaXRlO1xuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjMzc2ZjZmO1xufVxuXG5pb24tYnV0dG9uIHtcblx0LS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuaW9uLWljb257XG4gIGNvbG9yOiAjREZBNDNFO1xuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG59XG5cbmlvbi10ZXh0e1xuICBjb2xvcjogI0RGQTQzRTtcbn1cbiIsIi53ZWxjb21lLWNhcmQgaW1nIHtcbiAgbWF4LWhlaWdodDogMzV2aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLnRlc3QwMSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJlaWdlO1xufVxuXG4udGVzdDAyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmVpZ2U7XG59XG5cbmg1IHtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcbn1cblxuaW9uLWNhcmQge1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNzZmNmY7XG59XG5cbmlvbi1idXR0b24ge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG5pb24taWNvbiB7XG4gIGNvbG9yOiAjREZBNDNFO1xuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG59XG5cbmlvbi10ZXh0IHtcbiAgY29sb3I6ICNERkE0M0U7XG59Il19 */"

/***/ }),

/***/ "./src/app/tab1/tab1.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var Tab1Page = /** @class */ (function () {
    function Tab1Page(router) {
        this.router = router;
    }
    Tab1Page.prototype.navigate = function () {
        this.router.navigate(['(questionaries']);
    };
    Tab1Page.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab1',
            template: __webpack_require__(/*! raw-loader!./tab1.page.html */ "./node_modules/raw-loader/index.js!./src/app/tab1/tab1.page.html"),
            styles: [__webpack_require__(/*! ./tab1.page.scss */ "./src/app/tab1/tab1.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], Tab1Page);
    return Tab1Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es5.js.map