(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab5-tab5-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tab5/tab5.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tab5/tab5.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Xat\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <div class=\"container\">\n        <img src=\"../../assets/imgs/avatar01.png\" alt=\"Avatar\">\n        <p>Hola. Em podeu ajudar?</p>\n        <span class=\"time-right\">11:00</span>\n    </div>\n\n    <div class=\"container darker\">\n        <img src=\"../../assets/imgs/avatar02.png\" alt=\"Avatar\" class=\"right\">\n        <p>Hola, i tant! Què necessites?</p>\n        <span class=\"time-left\">11:01</span>\n    </div>\n\n    <div class=\"container\">\n        <img src=\"../../assets/imgs/avatar01.png\" alt=\"Avatar\">\n        <p> En quin contenidor puc tirar les piles?</p>\n        <span class=\"time-right\">11:02</span>\n    </div>\n\n    <div class=\"container darker\">\n        <img src=\"../../assets/imgs/avatar02.png\" alt=\"Avatar\" class=\"right\">\n        <p>Les piles convencionals s’han de llençar als contenidors específics per a la seva recollida </p>\n        <span class=\"time-left\">11:05</span>\n    </div>\n    <div class=\"input\">\n        <ion-input> </ion-input>\n    </div>\n    <ion-icon name=\"send\"></ion-icon>\n\n   <!-- <button>\n      Preguntes freqüents\n    </button>\n    <button >\n      Notificar incidència\n    </button>\n    <button [routerLink]=\"['/chat']\">\n      Obrir xat\n    </button> -->\n</ion-content>\n"

/***/ }),

/***/ "./src/app/tab5/tab5.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab5/tab5.module.ts ***!
  \*************************************/
/*! exports provided: Tab5PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab5PageModule", function() { return Tab5PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab5_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab5.page */ "./src/app/tab5/tab5.page.ts");







var Tab5PageModule = /** @class */ (function () {
    function Tab5PageModule() {
    }
    Tab5PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab5_page__WEBPACK_IMPORTED_MODULE_6__["Tab5Page"] }])
            ],
            declarations: [_tab5_page__WEBPACK_IMPORTED_MODULE_6__["Tab5Page"]]
        })
    ], Tab5PageModule);
    return Tab5PageModule;
}());



/***/ }),

/***/ "./src/app/tab5/tab5.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab5/tab5.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button {\n  width: 80%;\n  margin-left: 10%;\n  margin-top: 20px;\n  color: white;\n  background: #376f6f;\n  height: 50px;\n  border-radius: 15px;\n  font-weight: bold;\n}\n\n.input {\n  width: 80%;\n  margin-left: 5%;\n  border-radius: 15px;\n}\n\n/* Chat containers */\n\n.container {\n  border: 2px solid #dedede;\n  background-color: #f1f1f1;\n  border-radius: 5px;\n  padding: 10px;\n  width: 85%;\n  margin: 10px 0 10px 5%;\n}\n\n/* Darker chat container */\n\n.darker {\n  margin-left: 10%;\n  border-color: #ccc;\n  background-color: #ddd;\n}\n\n/* Clear floats */\n\n.container::after {\n  content: \"\";\n  clear: both;\n  display: table;\n}\n\n/* Style images */\n\n.container img {\n  float: left;\n  max-width: 60px;\n  width: 100%;\n  margin-right: 20px;\n  border-radius: 50%;\n}\n\n/* Style the right image */\n\n.container img.right {\n  float: right;\n  margin-left: 20px;\n  margin-right: 0;\n}\n\n/* Style time text */\n\n.time-right {\n  float: right;\n  color: #aaa;\n}\n\n/* Style time text */\n\n.time-left {\n  float: left;\n  color: #999;\n}\n\nion-input {\n  margin-top: 20px;\n  background: white;\n  height: 50px;\n  border-radius: 15px;\n  border: solid 1px blue;\n}\n\nion-icon {\n  display: block;\n  position: absolute;\n  top: 507px;\n  margin-left: 325px;\n  font-size: 33px;\n  color: blue;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZHJpYWNhcnJlcmEvR0lUL2Jsb2NrY2hhaW4tY2hhbGxlbmdlL3NyYy9hcHAvdGFiNS90YWI1LnBhZ2Uuc2NzcyIsInNyYy9hcHAvdGFiNS90YWI1LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNBRjs7QURJQTtFQUNFLFVBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNERjs7QURJQSxvQkFBQTs7QUFDQTtFQUNFLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0Esc0JBQUE7QUNERjs7QURJQSwwQkFBQTs7QUFDQTtFQUNFLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtBQ0RGOztBRElBLGlCQUFBOztBQUNBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0FDREY7O0FESUEsaUJBQUE7O0FBQ0E7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDREY7O0FESUEsMEJBQUE7O0FBQ0E7RUFDRSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDREY7O0FESUEsb0JBQUE7O0FBQ0E7RUFDRSxZQUFBO0VBQ0EsV0FBQTtBQ0RGOztBRElBLG9CQUFBOztBQUNBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7QUNERjs7QURJQTtFQUNFLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RGOztBRElBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNERiIsImZpbGUiOiJzcmMvYXBwL3RhYjUvdGFiNS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbmJ1dHRvbntcbiAgd2lkdGg6IDgwJTtcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiAjMzc2ZjZmO1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG5cbi5pbnB1dHtcbiAgd2lkdGg6IDgwJTtcbiAgbWFyZ2luLWxlZnQ6IDUlO1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xufVxuXG4vKiBDaGF0IGNvbnRhaW5lcnMgKi9cbi5jb250YWluZXIge1xuICBib3JkZXI6IDJweCBzb2xpZCAjZGVkZWRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjFmMWYxO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHdpZHRoOiA4NSU7XG4gIG1hcmdpbjogMTBweCAwIDEwcHggNSU7XG59XG5cbi8qIERhcmtlciBjaGF0IGNvbnRhaW5lciAqL1xuLmRhcmtlciB7XG4gIG1hcmdpbi1sZWZ0OiAxMCU7XG4gIGJvcmRlci1jb2xvcjogI2NjYztcbiAgYmFja2dyb3VuZC1jb2xvcjogI2RkZDtcbn1cblxuLyogQ2xlYXIgZmxvYXRzICovXG4uY29udGFpbmVyOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGNsZWFyOiBib3RoO1xuICBkaXNwbGF5OiB0YWJsZTtcbn1cblxuLyogU3R5bGUgaW1hZ2VzICovXG4uY29udGFpbmVyIGltZyB7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXgtd2lkdGg6IDYwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuLyogU3R5bGUgdGhlIHJpZ2h0IGltYWdlICovXG4uY29udGFpbmVyIGltZy5yaWdodCB7XG4gIGZsb2F0OiByaWdodDtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gIG1hcmdpbi1yaWdodDowO1xufVxuXG4vKiBTdHlsZSB0aW1lIHRleHQgKi9cbi50aW1lLXJpZ2h0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBjb2xvcjogI2FhYTtcbn1cblxuLyogU3R5bGUgdGltZSB0ZXh0ICovXG4udGltZS1sZWZ0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIGNvbG9yOiAjOTk5O1xufVxuXG5pb24taW5wdXR7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGJvcmRlcjogc29saWQgMXB4IGJsdWU7XG59XG5cbmlvbi1pY29ue1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwN3B4O1xuICBtYXJnaW4tbGVmdDogMzI1cHg7XG4gIGZvbnQtc2l6ZTogMzNweDtcbiAgY29sb3I6IGJsdWU7XG59IiwiYnV0dG9uIHtcbiAgd2lkdGg6IDgwJTtcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiAjMzc2ZjZmO1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uaW5wdXQge1xuICB3aWR0aDogODAlO1xuICBtYXJnaW4tbGVmdDogNSU7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG59XG5cbi8qIENoYXQgY29udGFpbmVycyAqL1xuLmNvbnRhaW5lciB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNkZWRlZGU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMWYxZjE7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogMTBweDtcbiAgd2lkdGg6IDg1JTtcbiAgbWFyZ2luOiAxMHB4IDAgMTBweCA1JTtcbn1cblxuLyogRGFya2VyIGNoYXQgY29udGFpbmVyICovXG4uZGFya2VyIHtcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgYm9yZGVyLWNvbG9yOiAjY2NjO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xufVxuXG4vKiBDbGVhciBmbG9hdHMgKi9cbi5jb250YWluZXI6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgY2xlYXI6IGJvdGg7XG4gIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4vKiBTdHlsZSBpbWFnZXMgKi9cbi5jb250YWluZXIgaW1nIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1heC13aWR0aDogNjBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufVxuXG4vKiBTdHlsZSB0aGUgcmlnaHQgaW1hZ2UgKi9cbi5jb250YWluZXIgaW1nLnJpZ2h0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tbGVmdDogMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAwO1xufVxuXG4vKiBTdHlsZSB0aW1lIHRleHQgKi9cbi50aW1lLXJpZ2h0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBjb2xvcjogI2FhYTtcbn1cblxuLyogU3R5bGUgdGltZSB0ZXh0ICovXG4udGltZS1sZWZ0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIGNvbG9yOiAjOTk5O1xufVxuXG5pb24taW5wdXQge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBib3JkZXI6IHNvbGlkIDFweCBibHVlO1xufVxuXG5pb24taWNvbiB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTA3cHg7XG4gIG1hcmdpbi1sZWZ0OiAzMjVweDtcbiAgZm9udC1zaXplOiAzM3B4O1xuICBjb2xvcjogYmx1ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/tab5/tab5.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab5/tab5.page.ts ***!
  \***********************************/
/*! exports provided: Tab5Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab5Page", function() { return Tab5Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var Tab5Page = /** @class */ (function () {
    function Tab5Page() {
    }
    Tab5Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab5',
            template: __webpack_require__(/*! raw-loader!./tab5.page.html */ "./node_modules/raw-loader/index.js!./src/app/tab5/tab5.page.html"),
            styles: [__webpack_require__(/*! ./tab5.page.scss */ "./src/app/tab5/tab5.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], Tab5Page);
    return Tab5Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab5-tab5-module-es5.js.map