(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab4-tab4-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tab4/tab4.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tab4/tab4.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Recompenses\n    </ion-title>\n      <ion-text slot=\"end\">Nivell 99</ion-text>\n      <ion-icon style=\"margin-left: 10px;\" slot=\"end\" src=\"assets/recycle-solid.svg\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <div  class=\"head\">\n        <p> Com més alt sigui el teu nivel ECO, millors seran les recompenses</p>\n    </div>\n    <ion-card>\n        <ion-list>\n            <ion-list-header> Descomptes al transport públic: </ion-list-header>\n            <ion-item (click)=\"showAlert('Utilitza el codi GHJKWM72D al web de bicing.cat')\">\n                <ion-icon name=\"bicycle\"></ion-icon>\n                <ion-label> Bicing </ion-label>\n                <ion-text class=\"achieved\"> Nivell 50 </ion-text>\n            </ion-item>\n            <ion-item (click)=\"showAlert('Utilitza el codi ASDTIC12 al web de rodalies.cat')\">\n                <ion-icon name=\"bus\"></ion-icon>\n                <ion-label> Rodalies </ion-label>\n                <ion-text> Nivell 100 </ion-text>\n            </ion-item>\n            <ion-item (click)=\"showAlert('Utilitza el codi PCBY23D al web de renfe.com')\">\n                <ion-icon name=\"bicycle\"></ion-icon>\n                <ion-label> Renfe Plata </ion-label>\n                <ion-text>  Nivell 150 </ion-text>\n            </ion-item>\n            <ion-item (click)=\"showAlert('Utilitza el codi UCYRBN1 al web de tmb.cat')\">\n                <ion-icon name=\"bus\"></ion-icon>\n                <ion-label> T-mes </ion-label>\n                <ion-text>  Nivell 200 </ion-text>\n            </ion-item>\n\n\n        </ion-list>\n    </ion-card>\n    <ion-card>\n        <ion-list>\n            <ion-list-header> Descomptes en activitats culturals: </ion-list-header>\n            <ion-item (click)=\"showAlert('Utilitza el codi SCYENC7 al web de mnac.cat')\">\n                <ion-icon name=\"bicycle\"></ion-icon>\n                <ion-label> Visita MNAC </ion-label>\n                <ion-text class=\"achieved\"> Nivell 35 </ion-text>\n            </ion-item>\n            <ion-item (click)=\"showAlert('Utilitza el codi 12SJCFG al web de sopars-de-carrer.cat')\">\n                <ion-icon name=\"bus\"></ion-icon>\n                <ion-label> Sopar popular </ion-label>\n                <ion-text class=\"achieved\"> Nivell 45 </ion-text>\n            </ion-item>\n            <ion-item (click)=\"showAlert('Utilitza el codi CKSYY25 al web de cosmocaixa.cat')\">\n                <ion-icon name=\"bicycle\"></ion-icon>\n                <ion-label> Cosmocaixa </ion-label>\n                <ion-text class=\"achieved\">  Nivell 70 </ion-text>\n            </ion-item>\n            <ion-item (click)=\"showAlert('Utilitza el codi POCH6E al web de sagradafamilia.cat')\">\n                <ion-icon name=\"bus\"></ion-icon>\n                <ion-label> Sagrada Familia </ion-label>\n                <ion-text>  Nivell 130 </ion-text>\n            </ion-item>\n\n\n        </ion-list>\n    </ion-card>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/tab4/tab4.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab4/tab4.module.ts ***!
  \*************************************/
/*! exports provided: Tab4PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4PageModule", function() { return Tab4PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab4_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab4.page */ "./src/app/tab4/tab4.page.ts");







var Tab4PageModule = /** @class */ (function () {
    function Tab4PageModule() {
    }
    Tab4PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab4_page__WEBPACK_IMPORTED_MODULE_6__["Tab4Page"] }])
            ],
            declarations: [_tab4_page__WEBPACK_IMPORTED_MODULE_6__["Tab4Page"]]
        })
    ], Tab4PageModule);
    return Tab4PageModule;
}());



/***/ }),

/***/ "./src/app/tab4/tab4.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab4/tab4.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-card {\n  margin-left: 5%;\n  width: 90%;\n  display: block;\n}\n\nion-icon {\n  color: #DFA43E;\n  margin-right: 15px;\n}\n\nion-text {\n  color: #DFA43E;\n}\n\n.achieved {\n  color: #376f6f;\n}\n\n.head {\n  color: black;\n  margin-left: 5%;\n  width: 90%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZHJpYWNhcnJlcmEvR0lUL2Jsb2NrY2hhaW4tY2hhbGxlbmdlL3NyYy9hcHAvdGFiNC90YWI0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvdGFiNC90YWI0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGVBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtBQ0NEOztBREVBO0VBQ0MsY0FBQTtFQUNBLGtCQUFBO0FDQ0Q7O0FERUE7RUFDQyxjQUFBO0FDQ0Q7O0FERUE7RUFDQyxjQUFBO0FDQ0Q7O0FERUE7RUFDQyxZQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUNDRCIsImZpbGUiOiJzcmMvYXBwL3RhYjQvdGFiNC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZHtcblx0bWFyZ2luLWxlZnQ6IDUlO1xuXHR3aWR0aDogOTAlO1xuXHRkaXNwbGF5OiBibG9jaztcbn1cblxuaW9uLWljb257XG5cdGNvbG9yOiAjREZBNDNFO1xuXHRtYXJnaW4tcmlnaHQ6IDE1cHg7XG59XG5cbmlvbi10ZXh0e1xuXHRjb2xvcjogI0RGQTQzRTtcbn1cblxuLmFjaGlldmVke1xuXHRjb2xvcjogIzM3NmY2Zjtcbn1cblxuLmhlYWR7XG5cdGNvbG9yOiBibGFjaztcblx0bWFyZ2luLWxlZnQ6IDUlO1xuXHR3aWR0aDogOTAlO1xufVxuIiwiaW9uLWNhcmQge1xuICBtYXJnaW4tbGVmdDogNSU7XG4gIHdpZHRoOiA5MCU7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG5pb24taWNvbiB7XG4gIGNvbG9yOiAjREZBNDNFO1xuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG59XG5cbmlvbi10ZXh0IHtcbiAgY29sb3I6ICNERkE0M0U7XG59XG5cbi5hY2hpZXZlZCB7XG4gIGNvbG9yOiAjMzc2ZjZmO1xufVxuXG4uaGVhZCB7XG4gIGNvbG9yOiBibGFjaztcbiAgbWFyZ2luLWxlZnQ6IDUlO1xuICB3aWR0aDogOTAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/tab4/tab4.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab4/tab4.page.ts ***!
  \***********************************/
/*! exports provided: Tab4Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4Page", function() { return Tab4Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var Tab4Page = /** @class */ (function () {
    function Tab4Page(alertController) {
        this.alertController = alertController;
    }
    Tab4Page.prototype.showAlert = function (message) {
        this.alertController.create({
            header: 'Aconseguit!',
            message: message,
            buttons: ['D\'acord']
        }).then(function (alert) { alert.present(); });
    };
    Tab4Page.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
    ]; };
    Tab4Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab4',
            template: __webpack_require__(/*! raw-loader!./tab4.page.html */ "./node_modules/raw-loader/index.js!./src/app/tab4/tab4.page.html"),
            styles: [__webpack_require__(/*! ./tab4.page.scss */ "./src/app/tab4/tab4.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
    ], Tab4Page);
    return Tab4Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab4-tab4-module-es5.js.map